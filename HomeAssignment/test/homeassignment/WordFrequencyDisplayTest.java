/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeassignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luiscarlosluquinperez
 */
public class WordFrequencyDisplayTest {
    
    public WordFrequencyDisplayTest() {
    }

    /**
     * Test of main method
     */
    @Test
    public void testMain() {
        String[] args = null;
        WordFrequencyDisplay.main(args);
    }

    /**
     * Test of countFrequency method
     */
    @Test
    public void testCountFrequency() {
        StringBuilder sb = new StringBuilder("hola mundo hola");
        HashMap<String,Word> hm = new HashMap<>();
        hm.put("hola", new Word("hola",2));
        hm.put("mundo", new Word("mundo",1));
        List<Word> expResult = new ArrayList<>(WordFrequencyDisplay.sort(hm));
        List<Word> result = WordFrequencyDisplay.countFrequency(sb);
        assertEquals(expResult, result);
    }

    /**
     * Test of sort method
     */
    @Test
    public void testSort() {
        HashMap<String,Word> hm = new HashMap<>();
        hm.put("hola", new Word("hola",2));
        hm.put("mundo", new Word("mundo",1));
        List<Word> listOfEntries = new ArrayList<>(hm.values());
        Collections.sort(listOfEntries);
        List<Word> expResult = listOfEntries;
        List<Word> result = WordFrequencyDisplay.sort(hm);
        assertEquals(expResult, result);
    }

    /**
     * Test of readFile method
     */
    @Test
    public void testReadFile() {
        String newSource = "";
        String expResult = "badger badger Badger mushroom,MUSHROOM. snake badger";
        String result = WordFrequencyDisplay.readFile(newSource).toString();
        assertEquals(expResult, result);
    }
    
}
