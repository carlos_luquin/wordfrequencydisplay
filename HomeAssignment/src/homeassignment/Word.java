/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeassignment;

import java.util.Objects;

/**
 *
 * @author Carlos
 */
public class Word implements Comparable<Object>{
    private String word;
    private Integer frequency;

    public String getWord() {
        return word;
    }

    public Word(String word, Integer frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Word() {
    }   

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.word);
        hash = 59 * hash + Objects.hashCode(this.frequency);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Word other = (Word) obj;
        if (!Objects.equals(this.word, other.word)) {
            return false;
        }
        if (!Objects.equals(this.frequency, other.frequency)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object o) {
        return (int) (((Word)o).frequency - this.frequency);
    }

    
    
}
