/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeassignment;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Carlos
 */
public class WordFrequencyDisplay {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*Leer archivo*/
        StringBuilder sb = readFile("");
        /*Contar frecuencia de apariciones y ordenar*/
        List<Word> resultado = countFrequency(sb);
        /*Imprimir resultados.*/
        for (Word entry : resultado) {
            System.out.print(entry.getWord()+ " ");
            for(int i = 1;i<=entry.getFrequency();i++){
                System.out.print("*");
            }
            System.out.println("");
        }
    }

    /*
    Method to count the frequency of every word in the file
    and order it descending.
    Params: StringBuilder sb, the file read
    Return: List<Word> ordered.
     */
    public static List<Word> countFrequency(StringBuilder sb) {
        HashMap<String, Word> lista = new HashMap<>();
        /*Quitar caracteres especiales y sustituirlos por espacios*/
        sb = new StringBuilder(sb.toString().replaceAll("[.,_:°!#$%&/()=?¡)]", " "));
        /*Separar la cadena en palabras, separarlas por cada espacio*/
        String[] valores = sb.toString().split(" ");
        for (String palabra : valores) {
            /*Si la palabra no es vacio o espacios y no se ha agregado en la lista 
            de palabras, entonces contar el número de apariciones.*/
            if (palabra != null && !palabra.trim().isEmpty()
                    && !lista.containsKey(palabra.toLowerCase())) {
                int cont = 0;
                for (String p : valores) {
                    if (palabra.toLowerCase().equalsIgnoreCase(p.toLowerCase())) {
                        cont++;
                    }
                }
                lista.put(palabra.toLowerCase(), new Word(palabra.toLowerCase(),cont) );
            }
        }
        return sort(lista);
    }

    /*
    Method to sort Collection
    Params: HashMap<String, Word> entries
    Return: List<Word> sorted
     */
    public static List<Word> sort(HashMap<String, Word> entries) {
        List<Word> listOfEntries = new ArrayList<>(entries.values());
        Collections.sort(listOfEntries);
        return listOfEntries;
    }

    /*
    Method to read the specific file by the new source, if the source 
    doesn't exist, then read the words.txt file.
    Params: String newSource(new file just in case)
    Return: A file read in a StringBuilder Object
     */
    public static StringBuilder readFile(String newSource) {
        String source = "./src/words.txt";
        /*Si se envía una nueva ruta del archivo, tomar el archivo
        de esta ruta, sino usar el archivo por default*/
        if (newSource != null && !newSource.trim().isEmpty()) {
            source = newSource;
        }
        BufferedReader archivo = null;
        StringBuilder sb = null;
        try {
            /*Obtener archivo*/
            archivo = new BufferedReader(new FileReader(source));
            sb = new StringBuilder();
            String linea;
            /*Leer archivo por líneas y agregarlo a un StringBuilder*/
            while ((linea = archivo.readLine()) != null) {
                sb.append(linea);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File not found.");
        } catch (IOException ex) {
            System.out.println("Error accesing to the file.");
        } finally {
            try {
                if (archivo != null) {
                    archivo.close();
                }
            } catch (IOException ex) {
                System.out.println("Error while closing the file.");
            }
        }
        return sb;
    }

}
